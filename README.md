# Concat Rdf Xml

## Description
Small utility xslt, with an ant wrapper, and config file.
Takes a input rdf-xml file, looks up it's current folder, and concatinates all files in current folder wiht given extensions regex.
The directory lookup is non-recursive.

Namespaces from all rdf:RDF root nodes are copied. If there are duplicate prefixes with different uris, a fatal error is thrown.
Makes no attempt to validate or correct input xml, copies as is.

## Dependencies
Current implementation uses Saxon EE features, file:list, and saxon:discard.
There exists open source equivalents (i.e use collections for directory-metadata and ubb extension functions), so with some additional efforts it could run with 
Saxon-HE.

## To run
Run `build.xml` after configuring local.properties.default > local.properties

### Configuration
There are four parameters for configuring the transformation

`extensions` regexp (xs:string) (default `(\.owl$|\.xml$|\.rdf)`) if not set 

`owl_name` no default, current xml:base and ontology settings are copied as is, if not used. Removes all ontology statements and replaces with new if set.

`owl:imports` no default, zero or one string, with uris seperated by space. In current implementation this has to be used in combination with `owl:name`.

`debug` default false boolean, messages used during creation to inspect current context.


 
